var mongoose = require( 'mongoose' );
var Engineer = mongoose.model( 'Engineer' );

// indexコントローラ
exports.index = function(req, res){

  // Engineerモデルのfindメソッドで一覧を取得
  Engineer.find({}, function(error, engineers) {
    if(!error){
	  res.render('index', {
	    title: 'エンジニア一覧',
            // engineersにはMongoDBから取得したデータが返されるので、そのままJadeに渡す
	    engineers: engineers
	  });
    }
  });
};
