var express = require('express');
var app = express();
var http = require('http');
var server = http.createServer(app);
var bodyParser = require('body-parser');

// mongooseの読み込み
var mongoose = require('mongoose');

// Modelの設定
var db = require('./model/database');

// Controllerの設定
var router = require('./router');
var engineer = require('./router/engineer');

// Viewの設定
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

// bodyParserの設定
app.use(bodyParser.urlencoded({
  extended: true
}));

// ルーティング設定
app.get('/', router.index);
app.get('/engineer/new', engineer.generate);
app.post('/engineer/new', engineer.postGenerate);

server.listen(3000);
