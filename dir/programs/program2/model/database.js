// Modelでもmongooseを読み込みます
const mongoose = require( 'mongoose' );

// MongoDBに接続
const mURI = 'mongodb://localhost/posts';
mongoose.connect(mURI);

// 接続イベントを利用してログ出力
mongoose.connection.on('connected', function () {
	console.log(`mongoose URI locates {mURI}`);
});

// スキーマの定義とEngineerモデルの作成
const postsSchema = new mongoose.Schema({
	// id: { type: Number, unique: true}
	name: String,
	comments: String,
	created: { type: Date, default: Date.now },
});
mongoose.model( 'Posts', postsSchema );
