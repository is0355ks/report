//コメント投稿用

const mongoose = require( 'mongoose' );
const Posts = mongoose.model( 'Posts' );


/*
 * @コメント入力フォームの描写
 * @GET時のアクション
 * @jadeの変数へデータを渡す
 * @引数 req,res
 * @返り値 なし
*/
exports.submit = function(req, res){
  res.render('posts-form', {
    title: 'コメント投稿フォーム',
    buttonName: "投稿"
  });
};

/*
 * @コメント投稿
 * @POST時のアクション
 * @フォームに書いたコメントと名前をDBに登録
 * @引数 req,res
 * @返り値 なし
*/
exports.postSubmit = function(req, res){
  Posts.create({
    name: req.body.Name,
    comments: req.body.Comments,
  }, function( error, posts ){
    if(error){
      console.log(error);
    }else{
      console.log(`Comments saved: ${posts}`);
      res.redirect( '/' );
    }
  });
  console.log(req.body);
};
