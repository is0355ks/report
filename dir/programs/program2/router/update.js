//コメント投稿用

const mongoose = require( 'mongoose' );
const Posts = mongoose.model( 'Posts' );

// Getリクエスト用
exports.show = function(req, res){
  res.render('posts-form-update.jade', {
    title: 'コメント更新フォーム',
    buttonName: "更新"
  });
};

// Post一時用
exports.showUpdate = function(req, res){
  res.render('update_form.jade', {
    title: '更新内容',
    beforeName: '',
    beforeComments: '',
  });
}


// Postリクエスト用
exports.postUpdate = function(req, res){
  Posts.update({name:req.body.name},{$set:{comments:req.body.comments}},
  function( error, posts ){
    if(error){
      console.log(error);
    }else{
      console.log(`Comments updated: ${posts}`);
      res.redirect( '/' );
    }
  });
  console.log(req.body);
};
