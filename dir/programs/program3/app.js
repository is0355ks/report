<<<<<<< HEAD
var http = require( 'http' ); // HTTPモジュール読み込み
var socketio = require( 'socket.io' ); // Socket.IOモジュール読み込み
var fs = require( 'fs' ); // ファイル入出力モジュール読み込み

// 3000番ポートでHTTPサーバーを立てる
var server = http.createServer( function( req, res ) {
		if (req.url == '/') {
    	res.writeHead(200, { 'Content-Type' : 'text/html' }); // ヘッダ出力
    	res.end( fs.readFileSync('./public/views/index.html', 'utf-8') );  // index.htmlの内容を出力
    }else if(req.url == '/css/style.css'){
			res.writeHead(200, { 'Content-Type' : 'text/css' }); // ヘッダ出力
    	res.end( fs.readFileSync('./public/styles/style.css', 'utf-8') );  // index.htmlの内容を出力
    }else if(req.url == '/add'){
    	res.writeHead(200,{ 'Content-Type' : 'text/html' });
    	res.end( fs.readFileSync('./public/views/add.html', 'utf-8') );  // index.htmlの内容を出力
    }else if(req.url == '/css/add_style.css'){
    	res.writeHead(200, { 'Content-Type' : 'text/css' }); // ヘッダ出力
    	res.end( fs.readFileSync('./public/styles/add_style.css', 'utf-8') );  // index.htmlの内容を出力
    }
}).listen(3000);

// サーバーをソケットに紐付ける
var io = socketio.listen( server );

// 接続確立後の通信処理部分を定義
io.sockets.on( 'connection', function( socket ) {
	var person = fs.readFileSync('./Person.txt','utf8');
	var personList = person.split(",");
  io.sockets.emit( 'personList', personList );
  // クライアントからサーバーへ メッセージ送信ハンドラ（自分を含む全員宛に送る）
  socket.on( 'c2s_message2', function( data ) {
    // サーバーからクライアントへ メッセージを送り返し
    io.sockets.emit( 's2c_message', data );
  });

  socket.on('addMember',function(data){
  	console.log(data);
  	fs.writeFile('./Person.txt',data,'utf8',function(err){
  		console.log(err);
  	});
  });
=======
var http = require( 'http' ); // HTTPモジュール読み込み
var request = require( 'request' ) //requestモジュール読み込み
var socketio = require( 'socket.io' ); // Socket.IOモジュール読み込み
var fs = require( 'fs' ); // ファイル入出力モジュール読み込み
var exec = require( 'child_process' ).exec;
var child;

var person = fs.readFileSync('./Person.txt','utf8');
var personList = person.split(",");

// 3000番ポートでHTTPサーバーを立てる
var server = http.createServer( function( req, res ) {
		if (req.url == '/') {
    	res.writeHead(200, { 'Content-Type' : 'text/html' }); // ヘッダ出力
    	res.end( fs.readFileSync('./public/views/index.html', 'utf-8') );  // index.htmlの内容を出力
    }else if(req.url == '/css/style.css'){
			res.writeHead(200, { 'Content-Type' : 'text/css' }); // ヘッダ出力
    	res.end( fs.readFileSync('./public/styles/style.css', 'utf-8') );  // index.htmlの内容を出力
    }else if(req.url == '/add'){
    	res.writeHead(200,{ 'Content-Type' : 'text/html' });
    	res.end( fs.readFileSync('./public/views/add.html', 'utf-8') );  // index.htmlの内容を出力
    }else if(req.url == '/css/add_style.css'){
    	res.writeHead(200, { 'Content-Type' : 'text/css' }); // ヘッダ出力
    	res.end( fs.readFileSync('./public/styles/add_style.css', 'utf-8') );  // index.htmlの内容を出力
    }
}).listen(3000);

// サーバーをソケットに紐付ける
var io = socketio.listen( server );

// 接続確立後の通信処理部分を定義
io.sockets.on( 'connection', function( socket ) {
  io.sockets.emit( 'personList', personList );

  socket.on('addMember',function(data){
  	console.log(data);
  	fs.writeFile('./Person.txt',data,'utf8',null);
  });

  socket.on('sendSlack',function(data){
    console.log(data);
    var isStatus = function(){
      if(data.status == 0){
        return '出社';
      }else{
        return '退社';
      }
    }
    var userStatus = isStatus();

    child = exec(`curl -X POST --data-urlencode 'payload={"channel": "#test", "username": "Manager", "text": "
      name: ${data.name}
      status: ${userStatus}
      ", "icon_emoji": ":ghost:"}' https://hooks.slack.com/services/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`);
  });    


>>>>>>> bc20edd8c9d5ecbe993258713c0f2b8a89b20130
});