//コメント削除用

const mongoose = require( 'mongoose' );
const Posts = mongoose.model( 'Posts' );

/*
 * @コメント削除フォームの描写
 * @GET時のアクション
 * @jadeの変数へデータを渡す
 * @引数 req,res
 * @返り値 なし
*/
exports.delete = function(req, res){
  res.render('posts-form', {
    title: 'コメント削除フォーム',
    buttonName: "削除"
  });
};

/*
 * @コメント削除
 * @POST時のアクション
 * @フォームに書いた名前のコメントがあった場合削除
 * @引数 req,res
 * @返り値 なし
*/
exports.postDelete = function(req, res){
  Posts.find({
    //これだと自分の投稿全て削除される
    name: req.body.Name,
  }).remove(function( error, posts ){
    if(error){
      console.log(error);
    }else{
      console.log(`Comments deleted: ${req.body.Name} ${req.body.Comments}`);
      res.redirect( '/' );
    }
  });
  console.log(req.body);
};
