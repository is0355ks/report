// Modelでもmongooseを読み込みます
var mongoose = require( 'mongoose' );

// MongoDBに接続
var mURI = 'mongodb://localhost/engineerblog';
mongoose.connect(mURI);

// 接続イベントを利用してログ出力
mongoose.connection.on('connected', function () {
  console.log('mongoose URI locates ' + mURI);
});

// スキーマの定義とEngineerモデルの作成
var engineerSchema = new mongoose.Schema({
  name: String,
  created: { type: Date, default: Date.now },
});
mongoose.model( 'Engineer', engineerSchema );
