// モジュールのインポート
const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const bodyParser = require('body-parser');
const async = require('async');

// mongooseの読み込み
const mongoose = require('mongoose');

// Modelの設定
const db = require('./model/database');

// Viewの設定
app.set('views','./views');
app.set('view engine', 'jade');

// bodyParserの設定
app.use(bodyParser.urlencoded({
  extended: true
}));

// Controllerの設定
// ルーティング設定
const router = require('./router');
app.get('/', router.index);

const add = require('./router/add');
app.get('/add', add.submit);
app.post('/add', add.postSubmit);

const rm = require('./router/rm');
app.get('/rm',rm.delete);
app.post('/rm',rm.postDelete);

/* 未実装
const update = require('./router/update');
app.get('/update',update.show);
app.post('/form_update',update.postUpdate);
*/

//テスト用
const test = require('./test');
app.get('/test',function(req,res){
	res.render('test',null);
});
app.post('/form_test',function(req,res){
	async.waterfall([
		function(callback){
			console.log(req.body);
		}
	]);
	res.redirect( '/' );
});

server.listen(3000);
