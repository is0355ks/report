const mongoose = require( 'mongoose' );
const Posts = mongoose.model( 'Posts' );

// indexコントローラ
exports.index = function(req, res){

  // Postsモデルのfindメソッドで一覧を取得
  Posts.find({}, function(error, posts) {
    if(!error){
  	  res.render('index', {
  	    title: 'コメント一覧',
        // postsにはMongoDBから取得したデータが返されるので、そのままJadeに渡す
  	    posts: posts
  	  });
    }
  });
};
