var mongoose = require( 'mongoose' );
var Engineer = mongoose.model( 'Engineer' );

// Getリクエスト用
exports.generate = function(req, res){
  res.render('engineer-form', {
    title: 'エンジニア登録フォーム',
    buttonName: "登録"
 });
};

// Postリクエスト用
exports.postGenerate = function(req, res){
  Engineer.create({
    name: req.body.Name,
  }, function( error, engineer ){
    if(error){
      console.log(error);
    }else{
      console.log("Engineer saved: " + engineer);
      res.redirect( '/' );
    }
  });
};
